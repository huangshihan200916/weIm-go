module weIm-go

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.1
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-redis/redis/v8 v8.8.2
	github.com/gorilla/websocket v1.4.2
	github.com/jianfengye/collection v1.3.2
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/olivere/elastic/v7 v7.0.24
	github.com/qiniu/go-sdk/v7 v7.9.5
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/viper v1.7.1
	go.etcd.io/etcd/client/v3 v3.5.0-alpha.0
	go.uber.org/zap v1.17.0
	golang.org/x/net v0.0.0-20210510120150-4163338589ed
	google.golang.org/grpc v1.37.1
	google.golang.org/protobuf v1.26.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
	gorm.io/driver/mysql v1.1.0
	gorm.io/gorm v1.21.10
)
