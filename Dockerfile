FROM golang:latest

ENV GOPROXY https://goproxy.cn,direct
ENV TZ=Asia/Shanghai
ENV GO111MODULE=on

WORKDIR /go/src/weIm-go
COPY . .

RUN go mod download

RUN go build -o main ./main.go

EXPOSE 20190 20191

CMD ["./main"]