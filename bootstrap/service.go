package boot

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/olivere/elastic/v7"
	"github.com/spf13/viper"
	etcd "go.etcd.io/etcd/client/v3"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"strings"
	"time"
	"weIm-go/app/global/lib"
	"weIm-go/app/service/es_svc"
	"weIm-go/app/utils/helper"
)

func LoadService() {
	InitLogger()
	connectDb()
	connectRedis()
	connectElasticsearch()
	connectEtcd()
	setQiNiu()
}

// InitLogger 初始化日志
func InitLogger() {
	writeSyncer := GetLogWriter()
	encoder := GetEncoder()
	core := zapcore.NewCore(encoder, writeSyncer, zapcore.DebugLevel)

	lib.Logger = zap.New(core, zap.AddCaller()).Sugar()
}

// 连接数据库
func connectDb() {
	//dbType := viper.GetString("db.dbType")
	host := viper.GetString("db.host")
	port := viper.GetString("db.port")
	user := viper.GetString("db.user")
	pass := viper.GetString("db.pass")
	dataBase := viper.GetString("db.dbname")
	charset := viper.GetString("db.charset")

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=True&loc=Local", user, pass, host, port, dataBase, charset)

	var err error
	lib.Db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
		SkipDefaultTransaction: true,
		PrepareStmt:            true,
		Logger:                 logger.Default.LogMode(logger.Info),
	})
	if err != nil {
		//gorm 数据库驱动初始化失败
		panic("连接数据库出错：" + err.Error())
	}
}

// 连接redis
func connectRedis() {
	host := viper.GetString("redis.host")
	port := viper.GetString("redis.port")
	pass := viper.GetString("redis.pass")
	lib.Redis = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", host, port),
		Password: pass,
		DB:       0,
	})
	_, err := lib.Redis.Ping(context.Background()).Result()
	if err != nil {
		panic("连接Redis出错：" + err.Error())
	}
}

// 连接es
func connectElasticsearch() {
	host := viper.GetString("elasticsearch.host")
	port := viper.GetString("elasticsearch.port")
	user := viper.GetString("elasticsearch.user")
	pass := viper.GetString("elasticsearch.pass")
	var err error
	lib.Elasticsearch, err = elastic.NewClient(
		elastic.SetSniff(false),
		elastic.SetURL("http://"+host+":"+port),
		elastic.SetBasicAuth(user, pass),
	)
	if err != nil {
		panic("连接Elasticsearch出错：" + err.Error())
	}

	// 创建用户位置索引
	err = es_svc.EsCurd.CreateIndexUserLocation()
	if err != nil {
		// Handle error
		panic(err)
	}
}

// 连接etcd
func connectEtcd() {
	etcdString := viper.GetString("cluster.etcd")
	etcdArr := strings.Split(etcdString, "|")

	var err error
	lib.Etcd, err = etcd.New(etcd.Config{
		Endpoints:   etcdArr,
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		panic("连接Etcd出错：" + err.Error())
	}
}

// 设置七牛云对象存储
func setQiNiu() {
	helper.QiNiuClient.AccessKey = viper.GetString("qiniu.accessKey")
	helper.QiNiuClient.SecretKey = viper.GetString("qiniu.secretKey")
	helper.QiNiuClient.Bucket = viper.GetString("qiniu.bucket")
	helper.QiNiuClient.Domain = viper.GetString("qiniu.domain")
}
