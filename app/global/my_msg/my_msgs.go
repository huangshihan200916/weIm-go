package my_msg

const (
	// ProcessKilled 进程被结束
	ProcessKilled string = "收到信号，进程被结束"

	// ServerOccurredErrorMsg 服务器代码发生错误
	ServerOccurredErrorMsg string = "服务器内部发生代码执行错误, "

	// CURD 常用业务状态码

	CurdStatusOkMsg         string = "Success"
	CurdCreatFailMsg        string = "新增失败"
	CurdUpdateFailMsg       string = "更新失败"
	CurdDeleteFailMsg       string = "删除失败"
	CurdSelectFailMsg       string = "查询无数据"
	CurdRegisterFailMsg     string = "注册失败"
	CurdLoginFailMsg        string = "登录失败"
	CurdRefreshTokenFailMsg string = "刷新Token失败"
	CurdNovelHitsFailMsg    string = "增加点击量失败"

	// Captcha

	CaptchaGetParamsInvalidMsg   string = "获取验证码：提交的验证码参数无效,请检查验证码ID以及文件名后缀是否完整"
	CaptchaCheckParamsInvalidMsg string = "校验验证码：提交的参数无效，请确保提交的验证码ID和值有效"
	CaptchaCheckOkMsg            string = "验证码校验通过"
	CaptchaCheckFailMsg          string = "验证码校验失败"

	// 系统部分

	ContainerKeyAlreadyExists string = "该键已经注册在容器中了"
	PublicNotExists           string = "public 目录不存在"
	ConfigYamlNotExists       string = "config.yml 配置文件不存在"
	ConfigGormNotExists       string = "gorm_v2.yml 配置文件不存在"
	StorageLogsNotExists      string = "storage/logs 目录不存在"
	ConfigInitFail            string = "初始化配置文件发生错误"
	SoftLinkCreateFail        string = "自动创建软连接失败,请以管理员身份运行客户端(开发环境为goland等，生产环境检查命令执行者权限)"
	SoftLinkDeleteFail        string = "删除软软连接失败"

	FuncEventAlreadyExists   string = "注册函数类事件失败，键名已经被注册"
	FuncEventNotRegister     string = "没有找到键名对应的函数"
	FuncEventNotCall         string = "注册的函数无法正确执行"
	BasePath                 string = "初始化项目根目录失败"
	NoAuthorization          string = "token鉴权未通过，请通过token授权接口重新获取token,"
	ParseTokenFail           string = "解析token失败"
	GormInitFail             string = "Gorm 数据库驱动、连接初始化失败"
	CasbinNoAuthorization    string = "Casbin 鉴权未通过，请在后台检查 casbin 设置参数"
	GormNotInitGlobalPointer string = "%s 数据库全局变量指针没有初始化，请在配置文件 Gormv2.yml 设置 Gormv2.%s.IsInitGolobalGormMysql = 1, 并且保证数据库配置正确 \n"

	// 数据库部分

	DbDriverNotExists   string = "数据库驱动类型不存在,目前支持的数据库类型：mysql、sqlserver、postgresql，您提交数据库类型"
	DialectorDbInitFail string = "gorm dialector 初始化失败"

	//redis部分

	RedisInitConnFail string = "初始化redis连接池失败"
	RedisAuthFail     string = "Redis Auth 鉴权失败，密码错误"
	RedisGetConnFail  string = "Redis 从连接池获取一个连接失败，超过最大重试次数"

	// 验证器错误

	ValidatorNotExists      string = "不存在的验证器"
	ValidatorBindParamsFail string = "验证器绑定参数失败"
	// ValidatorParamsCheckFailMsg 表单验证器前缀
	ValidatorParamsCheckFailMsg string = "参数校验失败"

	//token部分

	TokenInvalid         string = "无效的token"
	TokenNotActiveYet    string = "token 尚未激活"
	TokenMalFormed       string = "token 格式不正确"
	JwtTokenFormatErrMsg string = "提交的 token 格式错误" //提交的 token 格式错误

	//snowflake

	SnowflakeGetIdFail string = "获取snowflake唯一ID过程发生错误"

	// websocket

	WebsocketOnOpenFail                 string = "websocket onopen 发生阶段错误"
	WebsocketUpgradeFail                string = "websocket Upgrade 协议升级, 发生错误"
	WebsocketReadMessageFail            string = "websocket ReadPump(实时读取消息)协程出错"
	WebsocketBeatHeartFail              string = "websocket BeatHeart心跳协程出错"
	WebsocketBeatHeartsMoreThanMaxTimes string = "websocket BeatHeart 失败次数超过最大值"
	WebsocketSetWriteDeadlineFail       string = "websocket  设置消息写入截止时间出错"
	WebsocketWriteMgsFail               string = "websocket  Write Msg(send msg) 失败"
	WsServerNotStartMsg                 string = "websocket 服务没有开启，请在配置文件开启"
	WsOpenFailMsg                       string = "websocket open阶段初始化基本参数失败"

	// rabbitMq

	RabbitMqReconnectFail string = "RabbitMq消费者端掉线后重连失败，超过尝试最大次数"

	//文件上传

	FilesUploadOpenFail           string = "打开文件失败，详情"
	FilesUploadReadFail           string = "读取文件32字节失败，详情"
	FilesUploadFailMsg            string = "文件上传失败, 获取上传文件发生错误!"
	FilesUploadMoreThanMaxSizeMsg string = "长传文件超过系统设定的最大值,系统允许的最大值（M）"
	FilesUploadMimeTypeFailMsg    string = "文件mime类型不允许"

	// casbin 初始化可能的错误

	CasbinCanNotUseDbPtr         string = "casbin 的初始化基于gorm 初始化后的数据库连接指针，程序检测到 gorm 连接指针无效，请检查数据库配置！"
	CasbinCreateAdaptFail        string = "casbin NewAdapterByDBUseTableName 发生错误"
	CasbinCreateEnforcerFail     string = "casbin NewEnforcer 发生错误"
	CasbinNewModelFromStringFail string = "NewModelFromString 调用时出错"
)
