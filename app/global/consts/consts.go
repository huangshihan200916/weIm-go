package consts

// 这里定义的常量，一般是具有错误代码+错误说明组成，一般用于接口返回
const (
	// ValidatorPrefix 表单验证器前缀
	ValidatorPrefix string = "Form_Validator_"

	// CurdStatusOkCode CURD
	CurdStatusOkCode int = 200

	// JwtTokenOK token有效
	JwtTokenOK int = 200100

	// ValidatorParamsCheckOK 参数校验 有效
	ValidatorParamsCheckOK int = 200101

	// JwtTokenInvalid 无效的token
	JwtTokenInvalid int = 400100

	// JwtTokenExpired 过期的token
	JwtTokenExpired int = 400101

	// JwtTokenFormatErrCode 提交的 token 格式错误
	JwtTokenFormatErrCode int = 400102

	// CurdCreatFailCode 新增失败
	CurdCreatFailCode int = 400200

	// CurdUpdateFailCode 更新失败
	CurdUpdateFailCode int = 400201

	// CurdDeleteFailCode 删除失败
	CurdDeleteFailCode int = 400202

	// CurdSelectFailCode 查询无数据
	CurdSelectFailCode int = 400203

	// CurdRegisterFailCode 注册失败
	CurdRegisterFailCode int = 400204

	// CurdLoginFailCode 登录失败
	CurdLoginFailCode int = 400205

	// FilesUploadFailCode 文件上传失败, 获取上传文件发生错误!
	FilesUploadFailCode int = 400250

	// FilesUploadMoreThanMaxSizeCode 长传文件超过系统设定的最大值,系统允许的最大值（M）
	FilesUploadMoreThanMaxSizeCode int = 400251

	// FilesUploadMimeTypeFailCode 文件mime类型不允许
	FilesUploadMimeTypeFailCode int = 400252

	// WsServerNotStartCode websocket 服务没有开启，请在配置文件开启
	WsServerNotStartCode int = 400300

	// WsOpenFailCode websocket open阶段初始化基本参数失败
	WsOpenFailCode int = 400301

	// ValidatorParamsCheckFailCode 参数校验失败
	ValidatorParamsCheckFailCode int = 400500

	// InvalidParamsCheckFailCode 非法参数
	InvalidParamsCheckFailCode int = 400501

	// ServerOccurredErrorCode 服务器代码发生错误
	ServerOccurredErrorCode int = 500100
)
