package v1

import (
	"github.com/gin-gonic/gin"
	"weIm-go/app/global/consts"
	"weIm-go/app/http/validator/v1/valid_geo"
	"weIm-go/app/service/geo_svc"
	"weIm-go/app/utils/data_transfer"
	"weIm-go/app/utils/response"
)

type geo struct{}

var Geo geo

// Users 附近的人
func (geo) Users(ctx *gin.Context) {
	// 验证数据
	params, err := valid_geo.ValidGeoUser.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := geo_svc.GeoCurd.Users(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}
