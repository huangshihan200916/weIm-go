package v1

import (
	"github.com/gin-gonic/gin"
	"weIm-go/app/global/consts"
	"weIm-go/app/http/validator/v1/valid_group"
	"weIm-go/app/service/group_svc"
	"weIm-go/app/utils/data_transfer"
	"weIm-go/app/utils/response"
)

type group struct{}

var Group group

// Create 创建群组
func (group) Create(ctx *gin.Context) {
	// 验证数据
	params, err := valid_group.ValidGroupCreate.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := group_svc.GroupCurd.Create(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// Search 搜索群组
func (group) Search(ctx *gin.Context) {
	// 验证数据
	params, err := valid_group.ValidGroupSearch.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := group_svc.GroupCurd.Search(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// My 我的群组
func (group) My(ctx *gin.Context) {
	// 验证数据
	params, err := valid_group.ValidGroupMy.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := group_svc.GroupCurd.My(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// Join 申请加入群组
func (group) Join(ctx *gin.Context) {
	// 验证数据
	params, err := valid_group.ValidGroupJoin.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := group_svc.GroupCurd.Join(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// JoinList 申请加群列表
func (group) JoinList(ctx *gin.Context) {
	// 验证数据
	params, err := valid_group.ValidGroupJoinList.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := group_svc.GroupCurd.JoinList(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// JoinHandle 申请加群处理
func (group) JoinHandle(ctx *gin.Context) {
	// 验证数据
	params, err := valid_group.ValidGroupJoinHandle.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := group_svc.GroupCurd.JoinHandle(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// Leave 退出群组
func (group) Leave(ctx *gin.Context) {
	// 验证数据
	params, err := valid_group.ValidGroupLeave.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := group_svc.GroupCurd.Leave(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// Shot 踢出群组
func (group) Shot(ctx *gin.Context) {
	// 验证数据
	params, err := valid_group.ValidGroupShot.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := group_svc.GroupCurd.Shot(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}
