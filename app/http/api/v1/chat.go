package v1

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
	"weIm-go/app/global/consts"
	"weIm-go/app/global/lib"
	"weIm-go/app/http/validator/v1/valid_chat"
	"weIm-go/app/service/chat_svc"
	"weIm-go/app/utils/data_transfer"
	"weIm-go/app/utils/helper"
	"weIm-go/app/utils/response"
	"weIm-go/app/ws"
)

type chat struct{}

var Chat chat

// SendToUser 发送好友消息
func (chat) SendToUser(ctx *gin.Context) {
	// 验证数据
	params, err := valid_chat.ValidSendUser.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := chat_svc.ChatCurd.SendToUser(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// WithdrawFromUser 撤回好友消息
func (chat) WithdrawFromUser(ctx *gin.Context) {
	// 验证数据
	params, err := valid_chat.ValidWithdrawUser.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := chat_svc.ChatCurd.WithdrawFromUser(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// SendToGroup 发送群消息
func (chat) SendToGroup(ctx *gin.Context) {
	// 验证数据
	params, err := valid_chat.ValidSendGroup.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := chat_svc.ChatCurd.SendToGroup(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// WithdrawFromGroup 撤回群消息
func (chat) WithdrawFromGroup(ctx *gin.Context) {
	// 验证数据
	params, err := valid_chat.ValidWithdrawGroup.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := chat_svc.ChatCurd.WithdrawFromGroup(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// Upload 上传文件（聊天图片、语音、视频等）
func (chat) Upload(ctx *gin.Context) {
	file, err := ctx.FormFile("file")
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "请选择文件", nil)
		return
	}
	if file.Size/1024 > 20480 {
		response.Fail(ctx, consts.FilesUploadMoreThanMaxSizeCode, "文件太大", nil)
		return
	}

	// 校验图片格式
	messageType, _ := strconv.Atoi(ctx.PostForm("type"))
	fileExt := strings.ToLower(path.Ext(file.Filename))
	switch messageType {
	case ws.TypePicture:
		if fileExt != ".jpg" && fileExt != ".jpeg" && fileExt != ".bmp" && fileExt != ".png" && fileExt != ".gif" && fileExt != ".tif" && fileExt != ".webp" && fileExt != ".pcx" && fileExt != ".tga" && fileExt != ".exif" && fileExt != ".fpx" && fileExt != ".svg" && fileExt != ".wmf" {
			response.Fail(ctx, consts.FilesUploadMimeTypeFailCode, "图片格式不受支持", nil)
			return
		}
		break
	case ws.TypeVoice:
		if fileExt != ".mp3" && fileExt != ".wma" && fileExt != ".aac" && fileExt != ".rm" && fileExt != ".ra" && fileExt != ".rmx" && fileExt != ".wav" && fileExt != ".aiff" && fileExt != ".ogg" && fileExt != ".amr" && fileExt != ".ape" && fileExt != ".flac" {
			response.Fail(ctx, consts.FilesUploadMimeTypeFailCode, "语音格式不受支持", nil)
			return
		}
		break
	case ws.TypeVideo:
		if fileExt != ".avi" && fileExt != ".mp4" && fileExt != ".3gp" && fileExt != ".asf" && fileExt != ".wmv" && fileExt != ".rm" && fileExt != ".rmvb" && fileExt != ".flv" && fileExt != ".f4v" {
			response.Fail(ctx, consts.FilesUploadMimeTypeFailCode, "视频格式不受支持", nil)
			return
		}
		break
	case ws.TypeFile:
		break
	default:
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "请选择文件", nil)
		return
	}

	id := int(ctx.MustGet("id").(float64))
	now := time.Now().Unix()
	localPath := fmt.Sprintf("runtime/upload/%d_%d%s", id, now, fileExt)
	if err := ctx.SaveUploadedFile(file, localPath); err != nil {
		response.Fail(ctx, consts.FilesUploadFailCode, "上传错误", nil)
		lib.Logger.Debugf(err.Error())
		return
	}
	// 七牛云上传地址
	uploadPath := fmt.Sprintf("chat/%d_%d%s", id, now, fileExt)
	err = helper.QiNiuClient.Upload(localPath, uploadPath)
	// 删除本地缓存
	os.Remove(localPath)
	if err != nil {
		response.Fail(ctx, consts.ServerOccurredErrorCode, "服务器错误", nil)
		lib.Logger.Debugf(err.Error())
		return
	}

	response.Success(ctx, "上传成功", gin.H{
		"url": helper.QiNiuClient.FullPath(uploadPath),
	})
}
