package v1

import (
	"github.com/gin-gonic/gin"
	"weIm-go/app/global/consts"
	"weIm-go/app/http/validator/v1/valid_im"
	"weIm-go/app/service/im_svc"
	"weIm-go/app/utils/data_transfer"
	"weIm-go/app/utils/response"
)

type friend struct{}

var Friend friend

// Search 搜索好友
func (friend) Search(ctx *gin.Context) {
	// 验证数据
	params, err := valid_im.ValidSearchFriend.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := im_svc.ImCurd.Search(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// List 好友列表
func (friend) List(ctx *gin.Context) {
	// 验证数据
	params, err := valid_im.ValidListFriend.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := im_svc.ImCurd.List(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// Add 添加好友
func (friend) Add(ctx *gin.Context) {
	// 验证数据
	params, err := valid_im.ValidAddFriend.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := im_svc.ImCurd.Add(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// AddReqs 收到的好友请求列表
func (friend) AddReqs(ctx *gin.Context) {
	// 验证数据
	params, err := valid_im.ValidAddReqs.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := im_svc.ImCurd.AddReqs(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// AddHandle 处理收到的好友请求
func (friend) AddHandle(ctx *gin.Context) {
	// 验证数据
	params, err := valid_im.ValidAddHandle.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := im_svc.ImCurd.AddHandle(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// Del 删除好友
func (friend) Del(ctx *gin.Context) {
	// 验证数据
	params, err := valid_im.ValidDel.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := im_svc.ImCurd.Del(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}
