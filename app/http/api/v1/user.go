package v1

import (
	"github.com/gin-gonic/gin"
	"path"
	"strings"
	"weIm-go/app/global/consts"
	"weIm-go/app/http/validator/v1/valid_user"
	"weIm-go/app/service/user_svc"
	"weIm-go/app/utils/data_transfer"
	"weIm-go/app/utils/response"
)

type user struct{}

var User user

// UpdateNickname 用户更新昵称
func (user) UpdateNickname(ctx *gin.Context) {
	// 验证数据
	params, err := valid_user.ValidUpdateNickname.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := user_svc.UserCurd.UpdateNickname(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// UpdateAvatar 用户更新头像
func (user) UpdateAvatar(ctx *gin.Context) {
	file, err := ctx.FormFile("avatar")
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "请选择图片", nil)
		return
	}
	if file.Size/1024 > 2048 {
		response.Fail(ctx, consts.FilesUploadMoreThanMaxSizeCode, "文件太大", nil)
		return
	}

	// 保存头像文件，格式为id
	fileExt := strings.ToLower(path.Ext(file.Filename))
	if fileExt != ".jpg" && fileExt != ".jpeg" && fileExt != ".bmp" && fileExt != ".png" && fileExt != ".gif" && fileExt != ".tif" {
		response.Fail(ctx, consts.FilesUploadMimeTypeFailCode, "图片格式不受支持", nil)
		return
	}

	if data, code, err := user_svc.UserCurd.UpdateAvatar(ctx, file, fileExt); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// RealNameVerify 实名认证
func (user) RealNameVerify(ctx *gin.Context) {
	// 验证数据
	params, err := valid_user.ValidRealNameVerify.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := user_svc.UserCurd.RealNameVerify(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// UpdateLocation 更新最新地理位置及IP
func (user) UpdateLocation(ctx *gin.Context) {
	// 验证数据
	params, err := valid_user.ValidUpdateLocation.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := user_svc.UserCurd.UpdateLocation(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}
