package v1

import (
	"github.com/gin-gonic/gin"
	"weIm-go/app/global/consts"
	"weIm-go/app/http/validator/v1/valid_sign"
	"weIm-go/app/service/sign_svc"
	"weIm-go/app/utils/data_transfer"
	"weIm-go/app/utils/response"
)

type sign struct{}

var Sign sign

// Login 用户登录接口
func (sign) Login(ctx *gin.Context) {
	// 验证数据
	params, err := valid_sign.ValidLogin.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := sign_svc.SignCurd.Login(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}

// Register 用户注册接口
func (sign) Register(ctx *gin.Context) {
	// 验证数据 并返回绑定数据
	params, err := valid_sign.ValidRegister.CheckParams(ctx)
	if err != nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, err.Error(), nil)
		return
	}

	// 将验证器成员(字段)绑定到数据传输到上下文，方便控制器获取
	bindDataContext := data_transfer.DataAddContext(params, ctx)
	if bindDataContext == nil {
		response.Fail(ctx, consts.ValidatorParamsCheckFailCode, "表单验证器json化失败", nil)
		return
	}

	if data, code, err := sign_svc.SignCurd.Register(bindDataContext); err == nil {
		response.Success(ctx, "成功", data)
	} else {
		response.Fail(ctx, code, err.Error(), nil)
	}
}
