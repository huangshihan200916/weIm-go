package valid_geo

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidGeoUser = GeoUser{}

type GeoUser struct {
	Longitude float64 `form:"longitude" json:"longitude"  binding:"required,longitude"`
	Latitude  float64 `form:"latitude" json:"latitude"  binding:"required,latitude"`
	Distance  float64 `form:"distance" json:"distance"  binding:"omitempty,gt=0"` // 距离范围
	Page      float64 `form:"page" json:"page"  binding:"omitempty,gt=0"`         // 分页
}

func (p GeoUser) CheckParams(ctx *gin.Context) (GeoUser, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
