package valid_group

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidGroupShot = GroupShot{}

type GroupShot struct {
	UserId  float64 `form:"user_id" json:"user_id"  binding:"required"`
	GroupId float64 `form:"group_id" json:"group_id"  binding:"required"`
}

func (p GroupShot) CheckParams(ctx *gin.Context) (GroupShot, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
