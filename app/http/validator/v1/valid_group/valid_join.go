package valid_group

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidGroupJoin = GroupJoin{}

type GroupJoin struct {
	GroupId float64 `form:"group_id" json:"group_id"  binding:"required"`
}

func (p GroupJoin) CheckParams(ctx *gin.Context) (GroupJoin, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
