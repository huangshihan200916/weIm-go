package valid_group

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidGroupJoinList = GroupJoinList{}

type GroupJoinList struct {
}

func (p GroupJoinList) CheckParams(ctx *gin.Context) (GroupJoinList, error) {
	if err := ctx.ShouldBindQuery(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
