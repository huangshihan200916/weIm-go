package valid_group

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidGroupJoinHandle = GroupJoinHandle{}

type GroupJoinHandle struct {
	JoinId float64 `form:"join_id" json:"join_id"  binding:"required"`
	Status *int    `form:"status" json:"status"  binding:"required,oneof=0 1"`
}

func (p GroupJoinHandle) CheckParams(ctx *gin.Context) (GroupJoinHandle, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
