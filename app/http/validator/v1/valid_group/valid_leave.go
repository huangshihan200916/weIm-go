package valid_group

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidGroupLeave = GroupLeave{}

type GroupLeave struct {
	GroupId float64 `form:"group_id" json:"group_id"  binding:"required"`
}

func (p GroupLeave) CheckParams(ctx *gin.Context) (GroupLeave, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
