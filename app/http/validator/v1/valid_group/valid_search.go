package valid_group

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidGroupSearch = GroupSearch{}

type GroupSearch struct {
	Word string `form:"word" json:"word"  binding:"required"`
}

func (p GroupSearch) CheckParams(ctx *gin.Context) (GroupSearch, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
