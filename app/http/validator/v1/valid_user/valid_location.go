package valid_user

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidUpdateLocation = UpdateLocation{}

type UpdateLocation struct {
	Longitude float64 `form:"longitude" json:"longitude"  binding:"required,longitude"`
	Latitude  float64 `form:"latitude" json:"latitude"  binding:"required,latitude"`
}

func (p UpdateLocation) CheckParams(ctx *gin.Context) (UpdateLocation, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
