package valid_user

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	_ "weIm-go/app/http/validator/my_valid"
	"weIm-go/app/http/validator/translator"
)

var ValidRealNameVerify = RealNameVerify{}

// RealNameVerify 增加2个自定义验证ValidIsChinese, ValidCitizenNo
type RealNameVerify struct {
	RealName string `form:"real_name" json:"real_name"  binding:"required,min=2,max=20,ValidIsChinese"`
	IdCard   string `form:"id_card" json:"id_card"  binding:"required,ValidCitizenNo"`
}

func (p RealNameVerify) CheckParams(ctx *gin.Context) (RealNameVerify, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)
		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
