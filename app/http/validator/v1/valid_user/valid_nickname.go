package valid_user

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidUpdateNickname = UpdateNickname{}

type UpdateNickname struct {
	Nickname string `form:"nickname" json:"nickname"  binding:"required,min=4,max=20"`
}

func (p UpdateNickname) CheckParams(ctx *gin.Context) (UpdateNickname, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
