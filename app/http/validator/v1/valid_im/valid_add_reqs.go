package valid_im

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidAddReqs = AddReqs{}

type AddReqs struct {
}

func (p AddReqs) CheckParams(ctx *gin.Context) (AddReqs, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
