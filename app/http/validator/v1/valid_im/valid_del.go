package valid_im

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidDel = Del{}

type Del struct {
	FUid float64 `form:"f_uid" json:"f_uid"  binding:"required"`
}

func (p Del) CheckParams(ctx *gin.Context) (Del, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
