package valid_im

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidSearchFriend = SearchFriend{}

type SearchFriend struct {
	Word string `form:"word" json:"word"  binding:"required"`
}

func (p SearchFriend) CheckParams(ctx *gin.Context) (SearchFriend, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
