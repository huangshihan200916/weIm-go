package valid_im

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidAddFriend = AddFriend{}

type AddFriend struct {
	Username string `form:"username" json:"username"  binding:"required"`
	Channel  string `form:"channel" json:"channel"  binding:"required"`
	Reason   string `form:"reason" json:"reason"  binding:"required"`
}

func (p AddFriend) CheckParams(ctx *gin.Context) (AddFriend, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
