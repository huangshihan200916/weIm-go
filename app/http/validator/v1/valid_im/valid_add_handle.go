package valid_im

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidAddHandle = AddHandle{}

type AddHandle struct {
	FUid   float64 `form:"f_uid" json:"f_uid"  binding:"required"`
	Status int     `form:"status" json:"status"  binding:"required,oneof=1 -1"`
}

func (p AddHandle) CheckParams(ctx *gin.Context) (AddHandle, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
