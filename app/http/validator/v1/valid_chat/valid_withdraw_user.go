package valid_chat

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidWithdrawUser = WithdrawUser{}

type WithdrawUser struct {
	MessageId float64 `form:"message_id" json:"message_id" binding:"required"`
}

func (p WithdrawUser) CheckParams(ctx *gin.Context) (WithdrawUser, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
