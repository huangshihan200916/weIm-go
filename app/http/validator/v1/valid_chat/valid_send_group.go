package valid_chat

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidSendGroup = SendGroup{}

type SendGroup struct {
	GroupId float64 `form:"group_id" json:"group_id" binding:"required"`
	Type    *int    `form:"type" json:"type" binding:"required,oneof=0 1 2 3 4 6 10"`
	Body    string  `form:"body" json:"body" binding:"required,gte=1"`
}

func (p SendGroup) CheckParams(ctx *gin.Context) (SendGroup, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
