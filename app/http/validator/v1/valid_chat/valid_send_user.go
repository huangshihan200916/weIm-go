package valid_chat

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidSendUser = SendUser{}

type SendUser struct {
	ToUid float64 `form:"to_uid" json:"to_uid" binding:"required"`
	Type  *int    `form:"type" json:"type" binding:"required,oneof=0 1 2 3 4 6 10"`
	Body  string  `form:"body" json:"body" binding:"required,gte=1"`
}

func (p SendUser) CheckParams(ctx *gin.Context) (SendUser, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
