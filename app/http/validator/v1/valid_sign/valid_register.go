package valid_sign

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strings"
	"weIm-go/app/http/validator/translator"
)

var ValidRegister = Register{}

type Register struct {
	Username string `form:"username" json:"username"  binding:"required,min=6,max=20"`
	Password string `form:"password" json:"password" binding:"required,min=6,max=20"`
}

func (p Register) CheckParams(ctx *gin.Context) (Register, error) {
	if err := ctx.ShouldBind(&p); err != nil {
		trans := translator.TransferErr(err)

		return p, errors.New(strings.Join(trans, ","))
	}

	return p, nil
}
