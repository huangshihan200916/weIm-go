package translator

import (
	"fmt"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/locales/en"
	"github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	enTranslations "github.com/go-playground/validator/v10/translations/en"
	zhTranslations "github.com/go-playground/validator/v10/translations/zh"
	"reflect"
	"weIm-go/app/http/validator/my_valid"
)

// InitTrans 全局翻译器T
func InitTrans(locale string) (trans ut.Translator, err error) {
	//var trans ut.Translator
	//修改gin框架中Validator引擎属性，实现自定制
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		zhT := zh.New() //中文翻译器
		enT := en.New() //英文翻译器
		// 第一个参数是备用(fallback)语言环境
		// 后面参数是应该支持语言环境(可支持多个)
		uni := ut.New(enT, zhT)
		// locale通常取决于http请求'Accept-language'
		var ok bool
		trans, ok = uni.GetTranslator(locale)
		if !ok {
			return nil, fmt.Errorf("uni.GetTranslator(%s) failed", locale)
		}
		// 注册翻译器
		switch locale {
		case "en":
			err = enTranslations.RegisterDefaultTranslations(v, trans)
		case "zh":
			err = zhTranslations.RegisterDefaultTranslations(v, trans)
		default:
			err = enTranslations.RegisterDefaultTranslations(v, trans)
		}

		//注册一个函数，获取struct tag里自定义的label作为字段名
		v.RegisterTagNameFunc(func(fld reflect.StructField) string {
			name := fld.Tag.Get("label")
			return name
		})

		return
	}
	return
}

// TransferErr 把英文错误转成中文
func TransferErr(err error) []string {
	// 注册中文翻译器
	trans, _ := InitTrans("zh")

	var sliceErrs []string
	for _, err := range err.(validator.ValidationErrors) {
		if msg := my_valid.GetErrorMsg(err.Field(), err.Tag()); msg != "" {
			sliceErrs = append(sliceErrs, msg)
		} else {
			sliceErrs = append(sliceErrs, err.Translate(trans))
		}
	}

	return sliceErrs
}
