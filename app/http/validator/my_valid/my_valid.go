package my_valid

import (
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"strconv"
	"unicode"
	"weIm-go/app/utils/helper"
)

func init() {
	// 注册自定义验证函数
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		_ = v.RegisterValidation("ValidIsChinese", ValidIsChinese)
		_ = v.RegisterValidation("ValidCitizenNo", ValidCitizenNo)
	}
}

// ValidIsChinese  检测字符串是否含有中文
func ValidIsChinese(fl validator.FieldLevel) bool {
	// 获取传递过来的数据
	str := fl.Field().String()
	var count int
	for _, v := range str {
		if unicode.Is(unicode.Han, v) {
			count++
			break
		}
	}

	return count > 0
}

// ValidCitizenNo 校验身份证信息
func ValidCitizenNo(fl validator.FieldLevel) bool {
	// 获取传递过来的数据
	field := fl.Field().String()
	citizenNo := []byte(field)

	if !helper.IsValidCitizenNo18(&citizenNo) {
		return false
	}

	for i, v := range citizenNo {
		n, _ := strconv.Atoi(string(v))
		if n >= 0 && n <= 9 {
			continue
		}
		if v == 'X' && i == 16 {
			continue
		}
		return false
	}
	if !helper.CheckProvinceValid(citizenNo) {
		return false
	}
	nYear, _ := strconv.Atoi(string((citizenNo)[6:10]))
	nMonth, _ := strconv.Atoi(string((citizenNo)[10:12]))
	nDay, _ := strconv.Atoi(string((citizenNo)[12:14]))
	if !helper.CheckBirthdayValid(nYear, nMonth, nDay) {
		return false
	}
	return true
}

// GetErrorMsg 获取自定义错误信息
func GetErrorMsg(field, tag string) string {
	switch tag {
	case "ValidIsChinese":
		return field + ": 必须要输入中文"
	case "ValidCitizenNo":
		return field + ": 身份证不合法"
	}

	return ""
}
