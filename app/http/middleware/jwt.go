package middleware

import (
	"github.com/gin-gonic/gin"
	"weIm-go/app/global/consts"
	"weIm-go/app/service/jwt"
	"weIm-go/app/utils/response"
)

// ValidateJwtToken 检查Token是否有效
func ValidateJwtToken(ctx *gin.Context) {
	// 校验header中token值格式是否合法
	jwtToken := ctx.GetHeader("Login-Token")
	if len(jwtToken) < 16 {
		response.Fail(ctx, consts.JwtTokenInvalid, "鉴权失败", nil)
		ctx.Abort()
		return
	}

	_, err := jwt.ParseToken(ctx, jwtToken)
	if err != nil {
		// 解析失败，响应结束
		response.Fail(ctx, consts.JwtTokenInvalid, err.Error(), nil)
		ctx.Abort()
		return
	}
}
