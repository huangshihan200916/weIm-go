package user_svc

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"mime/multipart"
	"time"
	"weIm-go/app/global/consts"
	"weIm-go/app/global/lib"
	"weIm-go/app/model"
	"weIm-go/app/service/es_svc"
	"weIm-go/app/utils/helper"
)

var UserCurd = userCurd{}

type userCurd struct {
}

// UpdateNickname 用户更新昵称
func (r userCurd) UpdateNickname(ctx *gin.Context) (map[string]interface{}, int, error) {

	nickname := ctx.GetString(consts.ValidatorPrefix + "nickname")

	// jwt参数
	id := int(ctx.MustGet("id").(float64))
	// 更新用户昵称
	user := &model.User{Id: id}
	result := lib.Db.Model(&user).Update("nickname", nickname).RowsAffected

	return gin.H{"result": result}, consts.CurdStatusOkCode, nil
}

// UpdateAvatar 用户更新头像
func (r userCurd) UpdateAvatar(ctx *gin.Context, file *multipart.FileHeader, fileExt string) (map[string]interface{}, int, error) {
	id := int(ctx.MustGet("id").(float64))
	now := time.Now().Unix()

	// 本地缓存地址
	localPath := fmt.Sprintf("storage/upload/%d_%d%s", id, now, fileExt)
	if err := ctx.SaveUploadedFile(file, localPath); err != nil {
		lib.Logger.Debugf(err.Error())

		return nil, consts.FilesUploadFailCode, errors.New("上传错误")
	}

	// 七牛云上传地址   先注释，需要打开
	/*uploadPath := fmt.Sprintf("avatar/%d_%d%s", id, now, fileExt)
	err := helper.QiNiuClient.Upload(localPath, uploadPath)
	if err != nil {
		lib.Logger.Debugf(err.Error())

		return nil, consts.FilesUploadFailCode, errors.New("服务器错误")
	}*/

	// 删除本地缓存
	//os.Remove(localPath)

	var User model.User
	// 更新头像地址到数据库
	//user := &model.User{Avatar: uploadPath}
	user := &model.User{Avatar: localPath}

	result := lib.Db.Model(&User).Where("id = ?", id).Updates(user).RowsAffected

	return gin.H{"result": result}, consts.CurdStatusOkCode, nil
}

// RealNameVerify 实名认证
func (r userCurd) RealNameVerify(ctx *gin.Context) (map[string]interface{}, int, error) {
	realName := ctx.GetString(consts.ValidatorPrefix + "real_name")
	idCard := ctx.GetString(consts.ValidatorPrefix + "id_card")

	//校验身份证信息
	x := []byte(idCard)
	// 获取身份证信息：性别、生日、省份
	_, _, sex, _ := helper.GetCitizenNoInfo(x)
	uSex := 0
	if sex == "男" {
		uSex = 1
	} else if sex == "女" {
		uSex = 2
	}

	// jwt参数
	id := int(ctx.MustGet("id").(float64))
	// 更新实名信息
	user := &model.User{Id: id}
	result := lib.Db.Model(&user).Updates(map[string]interface{}{"real_name": realName, "id_card": idCard, "sex": uSex}).RowsAffected

	return gin.H{"result": result}, consts.CurdStatusOkCode, nil
}

// UpdateLocation 更新最新地理位置及IP
func (r userCurd) UpdateLocation(ctx *gin.Context) (map[string]interface{}, int, error) {
	Longitude := ctx.GetFloat64(consts.ValidatorPrefix + "longitude")
	Latitude := ctx.GetFloat64(consts.ValidatorPrefix + "latitude")

	id := int(ctx.MustGet("id").(float64))

	user := &model.User{Id: id}
	result := lib.Db.Model(&user).Updates(map[string]interface{}{"longitude": Longitude, "latitude": Latitude, "last_ip": ctx.ClientIP()}).RowsAffected

	// 更新经纬度到es，用于后期查询
	err := es_svc.EsCurd.InsertIndex(id, Latitude, Longitude)
	if err != nil {
		lib.Logger.Debugf(err.Error())
		return nil, consts.CurdCreatFailCode, errors.New("更新经纬度到es失败")
	}

	return gin.H{"result": result}, consts.CurdStatusOkCode, nil
}
