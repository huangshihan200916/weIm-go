package chat_svc

import (
	"errors"
	"github.com/gin-gonic/gin"
	"strconv"
	"time"
	"weIm-go/app/global/consts"
	"weIm-go/app/global/lib"
	"weIm-go/app/model"
	"weIm-go/app/ws"
)

var ChatCurd = chatCurd{}

type chatCurd struct {
}

// SendToUser 发送好友消息
func (r *chatCurd) SendToUser(ctx *gin.Context) (map[string]interface{}, int, error) {
	toUid := int(ctx.GetFloat64(consts.ValidatorPrefix + "to_uid"))
	messageType, _ := strconv.Atoi(ctx.GetString(consts.ValidatorPrefix + "type"))
	body := ctx.GetString(consts.ValidatorPrefix + "body")

	uid := int(ctx.MustGet("id").(float64))

	var friendList model.FriendList
	var count int64
	lib.Db.Where("uid = ? and f_uid = ?", uid, toUid).Select("id,uid,f_uid").First(&friendList).Count(&count)
	if count < 1 {
		return nil, consts.CurdSelectFailCode, errors.New("对方不是你的好友")
	}

	// 持久化消息记录
	chatMessage := model.ChatMessage{
		FromId:    uid,
		ToId:      toUid,
		Ope:       ws.OpeFriend,
		Type:      messageType,
		Body:      body,
		Status:    0,
		CreatedAt: time.Now().Unix(),
	}
	lib.Db.Create(&chatMessage)

	// 发送实时消息
	message := ws.Message{
		Cmd:    ws.CmdReceiveFriendMessage,
		FromId: uid,
		ToId:   toUid,
		Ope:    ws.OpeFriend,
		Type:   messageType,
		Body:   body,
	}
	ws.SendToUser(toUid, message)

	return gin.H{"id": chatMessage.Id}, consts.CurdStatusOkCode, nil
}

// WithdrawFromUser 撤回好友消息
func (r *chatCurd) WithdrawFromUser(ctx *gin.Context) (map[string]interface{}, int, error) {
	messageId := int(ctx.GetFloat64(consts.ValidatorPrefix + "message_id"))

	uid := int(ctx.MustGet("id").(float64))

	//查询消息记录
	var chatMessage model.ChatMessage
	lib.Db.Where("id = ? AND from_id = ? AND status <> ? AND ope = 0", messageId, uid, -1).First(&chatMessage)
	if chatMessage.Id == 0 {
		return nil, consts.CurdSelectFailCode, errors.New("消息不存在")
	}

	now := time.Now().Unix()
	// 判断消息发送是否超过2分钟
	if now > chatMessage.CreatedAt+120 {
		return nil, consts.ValidatorParamsCheckFailCode, errors.New("消息超过2分钟无法撤回")
	}

	// 把消息状态改为已撤回
	lib.Db.Model(&chatMessage).Updates(map[string]interface{}{"status": -1})

	// 通知对方撤回消息
	message := ws.Message{
		Cmd:    ws.CmdWithdrawFriendMessage,
		FromId: uid,
		ToId:   chatMessage.ToId,
		Ope:    ws.OpeFriend,
		Type:   ws.TypePrompt,
		Body:   strconv.Itoa(chatMessage.Id),
	}
	ws.SendToUser(chatMessage.ToId, message)

	return gin.H{}, consts.CurdStatusOkCode, nil
}

// SendToGroup 发送群消息
func (r *chatCurd) SendToGroup(ctx *gin.Context) (map[string]interface{}, int, error) {
	uid := int(ctx.MustGet("id").(float64))

	groupId := int(ctx.GetFloat64(consts.ValidatorPrefix + "group_id"))
	messageType, _ := strconv.Atoi(ctx.GetString(consts.ValidatorPrefix + "type"))
	body := ctx.GetString(consts.ValidatorPrefix + "body")

	var group model.Group
	lib.Db.Where("id = ?", groupId).First(&group)
	if group.Id < 1 {
		return nil, consts.ValidatorParamsCheckFailCode, errors.New("群组不存在")
	}

	var groupUser model.GroupUser
	lib.Db.Where("group_id = ? AND user_id = ?", groupId, uid).First(&groupUser)
	if groupUser.Id < 1 {
		return nil, consts.CurdSelectFailCode, errors.New("您不是群成员")
	}

	// 持久化消息记录
	chatGroup := model.ChatMessage{
		FromId:    uid,
		ToId:      groupId,
		Ope:       ws.OpeGroup,
		Type:      messageType,
		Body:      body,
		CreatedAt: time.Now().Unix(),
		Status:    0,
	}
	lib.Db.Create(&chatGroup)

	// 发送实时消息
	type result struct {
		UserId int
	}
	var groupUserList []result
	lib.Db.Raw("SELECT user_id FROM `group_user` WHERE group_id = >", groupId).Scan(&groupUserList)
	if len(groupUserList) > 0 {
		var userIdList []int
		for _, groupUser := range groupUserList {
			userIdList = append(userIdList, groupUser.UserId)
		}
		message := ws.Message{
			Cmd:    ws.CmdReceiveGroupMessage,
			FromId: uid,
			ToId:   groupId,
			Ope:    ws.OpeGroup,
			Type:   messageType,
			Body:   body,
		}
		ws.SendToGroup(userIdList, message)
	}

	return gin.H{"messageId": chatGroup.Id}, consts.CurdStatusOkCode, nil
}

// WithdrawFromGroup 撤回群消息
func (r *chatCurd) WithdrawFromGroup(ctx *gin.Context) (map[string]interface{}, int, error) {
	uid := int(ctx.MustGet("id").(float64))

	messageId := int(ctx.GetFloat64(consts.ValidatorPrefix + "message_id"))

	//查询消息记录
	var chatMessage model.ChatMessage
	lib.Db.Where("id = ? AND from_id = ? AND status <> ? AND ope = 1", messageId, uid, -1).First(&chatMessage)
	if chatMessage.Id < 1 {
		return nil, consts.CurdSelectFailCode, errors.New("消息不存在")
	}

	now := time.Now().Unix()
	// 判断消息发送是否超过2分钟
	if now > chatMessage.CreatedAt+120 {
		return nil, consts.ValidatorParamsCheckFailCode, errors.New("消息超过2分钟无法撤回")
	}

	// 把消息状态改为已撤回
	lib.Db.Model(&chatMessage).Updates(map[string]interface{}{"status": -1})

	// 发送实时消息
	type result struct {
		UserId int
	}
	var groupUserList []result
	lib.Db.Raw("SELECT user_id FROM `group_user` WHERE group_id = >", chatMessage.ToId).Scan(&groupUserList)
	if len(groupUserList) > 0 {
		var userIdList []int
		for _, groupUser := range groupUserList {
			userIdList = append(userIdList, groupUser.UserId)
		}
		message := ws.Message{
			Cmd:    ws.CmdWithdrawGroupMessage,
			FromId: uid,
			ToId:   chatMessage.ToId,
			Ope:    ws.OpeGroup,
			Type:   ws.TypePrompt,
			Body:   strconv.Itoa(chatMessage.Id),
		}
		ws.SendToGroup(userIdList, message)
	}

	return gin.H{}, consts.CurdStatusOkCode, nil
}
