package im_svc

import (
	"database/sql"
	"errors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"time"
	"weIm-go/app/global/consts"
	"weIm-go/app/global/lib"
	"weIm-go/app/model"
	"weIm-go/app/ws"
)

var ImCurd = imCurd{}

type imCurd struct {
}

type SearchUser struct {
	Username string
	Nickname string
	Avatar   string
	Sex      int
}

type List struct {
	Id       int
	Nickname string
	Username string
	Avatar   string
	Sex      int
}

type AddReq struct {
	Id        int
	Nickname  string
	Username  string
	Avatar    string
	Channel   string
	Reason    string
	RequestAt string
}

// Search 搜索好友
func (r *imCurd) Search(ctx *gin.Context) (map[string]interface{}, int, error) {
	word := ctx.GetString(consts.ValidatorPrefix + "word")
	uid := int(ctx.MustGet("id").(float64))

	var users []SearchUser
	lib.Db.Model(&model.User{}).
		Where("(username LIKE ? or nickname LIKE ?) and id <> ?", "%"+word+"%", "%"+word+"%", uid).
		Scan(&users)

	return gin.H{"list": users}, consts.CurdStatusOkCode, nil
}

// List 好友列表
func (r *imCurd) List(ctx *gin.Context) (map[string]interface{}, int, error) {
	uid := int(ctx.MustGet("id").(float64))

	var result List
	var results []List
	rows, err := lib.Db.Raw("select u.id,u.nickname,u.username,u.avatar,u.sex from friend_lists as f join users as u on f.f_uid = u.id where f.uid = ?", uid).Rows()
	if err != nil {
		lib.Logger.Debugf(err.Error())

		return nil, consts.ServerOccurredErrorCode, errors.New("服务器错误")
	}

	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			lib.Logger.Debugf(err.Error())
		}
	}(rows)

	for rows.Next() {
		err = rows.Scan(&result.Id, &result.Nickname, &result.Username, &result.Avatar, &result.Sex)
		results = append(results, result)
	}

	return gin.H{"lists": results}, consts.CurdStatusOkCode, nil
}

// Add 添加好友
func (r *imCurd) Add(ctx *gin.Context) (map[string]interface{}, int, error) {
	username := ctx.GetString(consts.ValidatorPrefix + "username")
	channel := ctx.GetString(consts.ValidatorPrefix + "channel")
	reason := ctx.GetString(consts.ValidatorPrefix + "reason")

	uid := int(ctx.MustGet("id").(float64))

	var user model.User
	var count int64
	lib.Db.Where("username = ?", username).Select("id,username").First(&user).Count(&count)
	if count < 1 {
		return nil, consts.CurdSelectFailCode, errors.New("用户未找到")
	}

	if user.Id == uid {
		return nil, consts.ValidatorParamsCheckFailCode, errors.New("不能添加自己")
	}

	var friendList model.FriendList
	lib.Db.Where("uid = ? and f_uid = ?", uid, user.Id).Select("id,uid,f_uid").First(&friendList).Count(&count)
	if count > 0 {
		return nil, consts.ValidatorParamsCheckFailCode, errors.New("对方已经是好友")
	}

	var friendAdd model.FriendAdd
	lib.Db.Where("uid = ? and f_uid = ? and status = 0", uid, user.Id).Select("id,uid,f_uid").First(&friendAdd).Count(&count)
	if count > 0 {
		return nil, consts.ValidatorParamsCheckOK, errors.New("请等待好友同意")
	}

	friendAdd.Uid = uid
	friendAdd.FUid = user.Id
	friendAdd.Reason = reason
	friendAdd.Channel = channel
	friendAdd.RequestAt = time.Now().Unix()
	if err := lib.Db.Create(&friendAdd).Error; err != nil {
		lib.Logger.Debugf(err.Error())
		return nil, consts.ServerOccurredErrorCode, errors.New("服务器错误")
	}

	// 实时通知用户添加请求
	message := ws.Message{
		Cmd:    ws.CmdReceiveFriendAdd,
		FromId: uid,
		ToId:   user.Id,
		Ope:    ws.OpeFriend,
		Type:   ws.TypePrompt,
		Body:   "您收到一条好友添加请求",
	}
	ws.SendToUser(user.Id, message)

	return gin.H{}, consts.CurdStatusOkCode, nil
}

// AddReqs 收到的好友请求列表
func (r *imCurd) AddReqs(ctx *gin.Context) (map[string]interface{}, int, error) {
	uid := int(ctx.MustGet("id").(float64))

	var result AddReq
	var results []AddReq
	rows, err := lib.Db.Raw("select u.id,u.nickname,u.username,u.avatar,f.channel,f.reason,f.request_at from friend_adds as f join users as u on f.uid = u.id  where f.f_uid = ? order by request_at desc", uid).Rows()
	if err != nil {
		lib.Logger.Debugf(err.Error())

		return nil, consts.ServerOccurredErrorCode, errors.New("服务器错误")
	}

	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			lib.Logger.Debugf(err.Error())
		}
	}(rows)

	for rows.Next() {
		err = rows.Scan(&result.Id, &result.Nickname, &result.Username, &result.Avatar, &result.Channel, &result.Reason, &result.RequestAt)
		results = append(results, result)
	}

	return gin.H{"list": results}, consts.CurdStatusOkCode, nil
}

// AddHandle 处理收到的好友请求
func (r *imCurd) AddHandle(ctx *gin.Context) (map[string]interface{}, int, error) {
	// 添加好友请求验证与数据写入
	fUid := int(ctx.GetFloat64(consts.ValidatorPrefix + "f_uid"))
	status := int(ctx.GetFloat64(consts.ValidatorPrefix + "status"))

	uid := int(ctx.MustGet("id").(float64))

	var friendAdd model.FriendAdd
	lib.Db.Where("uid = ? and f_uid = ? and status = ?", fUid, uid, 0).First(&friendAdd)

	if friendAdd.Id == 0 {
		return nil, consts.CurdSelectFailCode, errors.New("添加好友请求不存在")
	}
	now := time.Now().Unix()
	lib.Db.Model(&friendAdd).Updates(map[string]interface{}{"status": status, "pass_at": now})
	if status == -1 {
		return gin.H{}, consts.CurdStatusOkCode, nil
	}

	// 通过验证后进行好友数据写入
	friendList1 := &model.FriendList{
		Uid:       uid,
		FUid:      fUid,
		Channel:   friendAdd.Channel,
		Reason:    friendAdd.Reason,
		Role:      1,
		CreatedAt: now,
	}
	friendList2 := &model.FriendList{
		Uid:       fUid,
		FUid:      uid,
		Channel:   friendAdd.Channel,
		Reason:    friendAdd.Reason,
		Role:      2,
		CreatedAt: now,
	}
	// 事务
	err := lib.Db.Transaction(func(tx *gorm.DB) error {

		if err := tx.Create(&friendList1).Error; err != nil {
			// 返回任何错误都会回滚事务
			return err
		}

		if err := tx.Create(&friendList2).Error; err != nil {
			// 返回任何错误都会回滚事务
			return err
		}

		// 返回 nil 提交事务
		return nil
	})
	if err != nil {
		return nil, consts.CurdCreatFailCode, errors.New("数据写入错误")
	}

	// 实时通知对方通过了好友请求
	message := ws.Message{
		Cmd:    ws.CmdReceiveFriendAddResult,
		FromId: uid,
		ToId:   fUid,
		Ope:    ws.OpeFriend,
		Type:   ws.TypePrompt,
		Body:   "对方通过了你的好友请求",
	}
	ws.SendToUser(fUid, message)

	return gin.H{}, consts.CurdStatusOkCode, nil
}

// Del 删除好友
func (r *imCurd) Del(ctx *gin.Context) (map[string]interface{}, int, error) {
	fUid := int(ctx.GetFloat64(consts.ValidatorPrefix + "f_uid"))

	uid := int(ctx.MustGet("id").(float64))

	var friendList model.FriendList
	lib.Db.Where("uid = ? and f_uid = ?", uid, fUid).First(&friendList)
	if friendList.Id == 0 {
		return nil, consts.CurdSelectFailCode, errors.New("对方不是你的好友")
	}

	err := lib.Db.Delete(&friendList).Error
	if err != nil {
		lib.Logger.Debugf(err.Error())

		return nil, consts.CurdDeleteFailCode, errors.New("删除失败")
	}

	return gin.H{}, consts.CurdStatusOkCode, nil
}
