package jwt

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"time"
	"weIm-go/app/global/lib"
)

// MakeJwtToken 生成用户token
func MakeJwtToken(id int) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id": id,
		"at": time.Now().Unix(), // token有效期：一天
	})
	tokenString, err := token.SignedString(lib.JwtHmacSampleSecret)
	if err != nil {
		lib.Logger.Debugf(err.Error())
	}

	return tokenString
}

// ParseToken 解析token
func ParseToken(ctx *gin.Context, jwtToken string) (int, error) {
	token, err := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New(fmt.Sprintf("Unexpected signing method: %v", token.Header["alg"]))
		}
		return lib.JwtHmacSampleSecret, nil
	})
	if err != nil {
		return 0, err
	}

	// 开始解析token
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		// 解析成功
		ctx.Set("id", claims["id"])
		ctx.Set("at", claims["at"])
		at := int64(ctx.MustGet("at").(float64)) // token生成时间戳
		id := int(ctx.MustGet("id").(float64))
		nowAt := time.Now().Unix() // 当前时间戳
		expireAt := at + 7*24*3600 // 过期时间戳，一周后
		if nowAt > expireAt {
			return 0, errors.New("登录状态已过期，请重新登录")
		} else {
			return id, nil
		}
	} else {
		return 0, errors.New("鉴权失败")
	}
}
