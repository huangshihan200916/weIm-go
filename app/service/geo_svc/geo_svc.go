package geo_svc

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/olivere/elastic/v7"
	"strconv"
	"weIm-go/app/global/consts"
	"weIm-go/app/global/lib"
	"weIm-go/app/model"
	"weIm-go/app/utils/helper"
)

var GeoCurd = geoCurd{}

type geoCurd struct {
}

// Users 附近的人
func (s geoCurd) Users(ctx *gin.Context) (map[string]interface{}, int, error) {
	longitude := ctx.GetFloat64(consts.ValidatorPrefix + "longitude")
	latitude := ctx.GetFloat64(consts.ValidatorPrefix + "latitude")
	distance := ctx.GetFloat64(consts.ValidatorPrefix + "distance")
	page := ctx.GetFloat64(consts.ValidatorPrefix + "page")

	// 距离范围，默认100
	if distance < 1 {
		distance = 100
	}
	// 分页
	if page < 1 {
		page = 1
	}
	pageInt := int(page)
	size := 20
	from := (pageInt - 1) * size

	res, err := s.QueryByGeoDistance(distance, latitude, longitude, from, size)
	if err != nil {
		lib.Logger.Debugf(err.Error())

		return nil, consts.CurdSelectFailCode, err
	}

	// es数组变量
	var data []map[string]interface{}

	// 循环es结果
	uid := int(ctx.MustGet("id").(float64))

	for _, hit := range res.Hits.Hits {

		var userLocation model.UserLocation
		err := json.Unmarshal(hit.Source, &userLocation) // json解析结果
		if err != nil {
			lib.Logger.Debugf(err.Error())

			return nil, consts.ServerOccurredErrorCode, errors.New("服务器错误")
		}

		var user model.User
		lib.Db.Where("id = ?", userLocation.Uid).First(&user)
		// 列表中排除自己
		if user.Id == uid {
			continue
		}

		// 距离换算公里/米
		distance, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", hit.Sort[0]), 64)
		var distanceEcho string
		if distance < 1 {
			distanceEcho = fmt.Sprintf("%dm", int(distance*1000))
		} else {
			distanceEcho = fmt.Sprintf("%.2fkm", distance)
		}

		data = append(data, map[string]interface{}{
			"id":       user.Id,
			"username": user.Username,
			"nickname": user.Nickname,
			"avatar":   helper.QiNiuClient.FullPath(user.Avatar),
			"sex":      user.Sex,
			"distance": distanceEcho,
		})
	}

	return gin.H{
		"list": data,
	}, consts.CurdStatusOkCode, nil

}

// QueryByGeoDistance 根据geo查询
func (s *geoCurd) QueryByGeoDistance(distance, latitude, longitude float64, from, size int) (*elastic.SearchResult, error) {
	q := elastic.NewGeoDistanceQuery("pin.location").
		GeoPoint(elastic.GeoPointFromLatLon(latitude, longitude)).
		Distance(strconv.Itoa(int(distance)) + "km")

	sort := elastic.NewGeoDistanceSort("pin.location").
		Point(latitude, longitude).
		Unit("km").
		DistanceType("arc").
		Asc()

	return lib.Elasticsearch.Search(model.UserLocationIndexName).
		Query(q).SortBy(sort).From(from).Size(size).Do(context.Background())
}
