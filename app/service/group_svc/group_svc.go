package group_svc

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"time"
	"weIm-go/app/global/consts"
	"weIm-go/app/global/lib"
	"weIm-go/app/model"
	"weIm-go/app/ws"
)

var GroupCurd = groupCurd{}

type groupCurd struct {
}

type MyResult struct {
	Id        int
	OUid      int
	Name      string
	CreatedAt int
	JoinAt    int
}

type JoinListResult struct {
	JoinId    int
	UserId    int
	GroupId   int
	Username  string
	Nickname  string
	Avatar    string
	GroupName string
	JoinAt    int
}

// Create 创建群组
func (r *groupCurd) Create(ctx *gin.Context) (map[string]interface{}, int, error) {
	uid := int(ctx.MustGet("id").(float64))
	name := ctx.GetString(consts.ValidatorPrefix + "name")

	var group model.Group
	var count int64
	lib.Db.Where("name = ?", name).First(&group).Count(&count)
	if count > 0 {
		return nil, consts.CurdSelectFailCode, errors.New("群组已存在")
	}

	now := time.Now().Unix()
	// 事务
	err := lib.Db.Transaction(func(tx *gorm.DB) error {
		group.OUid = uid
		group.Name = name
		group.CreatedAt = now
		if err := tx.Create(&group).Error; err != nil {
			// 返回任何错误都会回滚事务
			return err
		}

		var groupUser model.GroupUser
		groupUser.GroupId = group.Id
		groupUser.UserId = uid
		groupUser.JoinAt = now
		if err := tx.Create(&groupUser).Error; err != nil {
			// 返回任何错误都会回滚事务
			return err
		}

		// 返回 nil 提交事务
		return nil
	})
	if err != nil {
		return nil, consts.CurdCreatFailCode, errors.New("创建失败")
	}

	return gin.H{}, consts.CurdStatusOkCode, nil
}

// Search 搜索群组
func (r *groupCurd) Search(ctx *gin.Context) (map[string]interface{}, int, error) {
	word := ctx.GetString(consts.ValidatorPrefix + "word")

	var groups []model.Group
	lib.Db.Model(&model.Group{}).Where("name LIKE ?", "%"+word+"%").Find(&groups)

	return gin.H{"list": groups}, consts.CurdStatusOkCode, nil
}

// My 我的群组
func (r *groupCurd) My(ctx *gin.Context) (map[string]interface{}, int, error) {
	uid := int(ctx.MustGet("id").(float64))

	var result []MyResult
	err := lib.Db.Raw("SELECT g.id,g.o_uid,g.name,g.created_at,u.join_at FROM `group_users` AS u INNER JOIN `groups` AS g ON u.group_id = g.id WHERE user_id = ?", uid).
		Scan(&result).Error
	if err != nil {
		lib.Logger.Debugf(err.Error())
		return nil, consts.ServerOccurredErrorCode, errors.New("服务器错误")
	}

	return gin.H{"list": result}, consts.CurdStatusOkCode, nil
}

// Join 申请加入群组
func (r *groupCurd) Join(ctx *gin.Context) (map[string]interface{}, int, error) {
	uid := int(ctx.MustGet("id").(float64))

	groupId := int(ctx.GetFloat64(consts.ValidatorPrefix + "group_id"))

	var group model.Group
	lib.Db.Where("id = ?", groupId).First(&group)
	if group.Id == 0 {
		return nil, consts.CurdSelectFailCode, errors.New("群组不存在")
	}
	if group.OUid == uid {
		return nil, consts.ValidatorParamsCheckFailCode, errors.New("你已经是群主")
	}

	var groupUser model.GroupUser
	lib.Db.Where("group_id = ? and user_id = ?", groupId, uid).First(&groupUser)
	if groupUser.Id > 0 {
		return nil, consts.ValidatorParamsCheckFailCode, errors.New("你已经在群组里")
	}

	var groupJoin model.GroupJoin
	lib.Db.Where("group_id = ? and user_id = ?", groupId, uid).First(&groupJoin)
	if groupJoin.Id > 0 {
		return nil, consts.ValidatorParamsCheckOK, errors.New("你的申请加入群组请求已经在处理中")
	}

	groupJoin.GroupId = groupId
	groupJoin.UserId = uid
	groupJoin.JoinAt = time.Now().Unix()
	if err := lib.Db.Create(&groupJoin).Error; err != nil {
		return nil, consts.CurdCreatFailCode, errors.New("申请失败")
	}

	// 实时通知群组
	message := ws.Message{
		Cmd:    ws.CmdReceiveGroupJoin,
		FromId: uid,
		ToId:   group.Id,
		Ope:    ws.OpeGroup,
		Type:   ws.TypePrompt,
		Body:   "对方申请加入群组",
	}
	ws.SendToUser(group.OUid, message)

	return gin.H{}, consts.CurdStatusOkCode, nil
}

// JoinList 申请加群列表
func (r *groupCurd) JoinList(ctx *gin.Context) (map[string]interface{}, int, error) {
	uid := int(ctx.MustGet("id").(float64))

	var result []JoinListResult
	err := lib.Db.Raw("SELECT j.id AS join_id,j.user_id,j.group_id,j.join_at,g.name AS group_name,u.username,u.nickname,u.avatar FROM `groups` AS g INNER JOIN `group_joins` AS j INNER JOIN `users` AS u ON g.id = j.group_id AND j.user_id = u.id WHERE g.o_uid = ? ORDER BY j.join_at", uid).
		Scan(&result).Error
	if err != nil {
		lib.Logger.Debugf(err.Error())
		return nil, consts.ServerOccurredErrorCode, errors.New("服务器错误")
	}

	return gin.H{}, consts.CurdStatusOkCode, nil
}

// JoinHandle 申请加群处理
func (r *groupCurd) JoinHandle(ctx *gin.Context) (map[string]interface{}, int, error) {
	joinId := int(ctx.GetFloat64(consts.ValidatorPrefix + "join_id"))
	status := int(ctx.GetFloat64(consts.ValidatorPrefix + "status"))

	uid := int(ctx.MustGet("id").(float64))

	var groupJoin model.GroupJoin
	lib.Db.Where("id = ?", joinId).First(&groupJoin)
	if groupJoin.Id == 0 {
		return nil, consts.CurdSelectFailCode, errors.New("加群申请不存在")
	}

	var group model.Group
	lib.Db.Where("id = ?", groupJoin.GroupId).First(&group)
	if group.Id == 0 {
		return nil, consts.CurdSelectFailCode, errors.New("群组不存在")
	}
	if group.OUid != uid {
		return nil, consts.CurdSelectFailCode, errors.New("您不是群组，无法处理申请")
	}

	// status 1同意加群 0拒绝加群
	if status == 1 {
		var groupUser model.GroupUser
		groupUser.GroupId = groupJoin.GroupId
		groupUser.UserId = groupJoin.UserId
		groupUser.JoinAt = time.Now().Unix()

		// 事务
		err := lib.Db.Transaction(func(tx *gorm.DB) error {
			if err := tx.Create(&groupUser).Error; err != nil {
				return err
			}

			if err := tx.Delete(&groupJoin).Error; err != nil {
				return err
			}

			return nil
		})
		if err != nil {
			return nil, consts.CurdCreatFailCode, err
		}

		// 实时通知加入群组
		message := ws.Message{
			Cmd:    ws.CmdReceiveGroupJoinResult,
			FromId: groupJoin.GroupId,
			ToId:   groupJoin.UserId,
			Ope:    ws.OpeGroup,
			Type:   ws.TypePrompt,
			Body:   "群主同意您加入群组",
		}
		ws.SendToUser(groupJoin.UserId, message)

		return gin.H{}, consts.CurdStatusOkCode, nil
	} else {
		lib.Db.Delete(&groupJoin)

		return gin.H{}, consts.CurdStatusOkCode, nil
	}
}

// Leave 退出群组
func (r *groupCurd) Leave(ctx *gin.Context) (map[string]interface{}, int, error) {
	uid := int(ctx.MustGet("id").(float64))

	groupId := int(ctx.GetFloat64(consts.ValidatorPrefix + "group_id"))

	var groupUser model.GroupUser
	lib.Db.Where("user_id = ? AND group_id = ?", uid, groupId).First(&groupUser)
	if groupUser.Id == 0 {
		return nil, consts.CurdSelectFailCode, errors.New("你不在群里")
	}

	lib.Db.Delete(&groupUser)

	return gin.H{}, consts.CurdStatusOkCode, nil
}

// Shot 踢出群组
func (r *groupCurd) Shot(ctx *gin.Context) (map[string]interface{}, int, error) {
	uid := int(ctx.MustGet("id").(float64))

	userId := int(ctx.GetFloat64(consts.ValidatorPrefix + "user_id"))
	groupId := int(ctx.GetFloat64(consts.ValidatorPrefix + "group_id"))

	var group model.Group
	lib.Db.Where("id = ?", groupId).First(&group)
	if group.Id == 0 {
		return nil, consts.CurdSelectFailCode, errors.New("群组不存在")
	}
	if group.OUid != uid {
		return nil, consts.CurdSelectFailCode, errors.New("你不是群主")
	}

	var groupUser model.GroupUser
	lib.Db.Where("user_id = ? AND group_id = ?", userId, groupId).First(&groupUser)
	if groupUser.Id == 0 {
		return nil, consts.CurdSelectFailCode, errors.New("用户不在群里")
	}

	lib.Db.Delete(&groupUser)

	// 实时通知用户被踢出群组
	message := ws.Message{
		Cmd:    ws.CmdReceiveGroupShot,
		FromId: groupId,
		ToId:   userId,
		Ope:    ws.OpeGroup,
		Type:   ws.TypePrompt,
		Body:   "你被踢出群组",
	}
	ws.SendToUser(userId, message)

	return gin.H{}, consts.CurdStatusOkCode, nil
}
