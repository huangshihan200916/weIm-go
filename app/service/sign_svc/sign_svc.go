package sign_svc

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"time"
	"weIm-go/app/global/consts"
	"weIm-go/app/global/lib"
	"weIm-go/app/model"
	"weIm-go/app/service/jwt"
	"weIm-go/app/utils/helper"
)

var SignCurd = signCurd{}

type signCurd struct {
}

// Register 用户注册
func (s signCurd) Register(ctx *gin.Context) (map[string]interface{}, int, error) {

	username := ctx.GetString(consts.ValidatorPrefix + "username")
	password := ctx.GetString(consts.ValidatorPrefix + "password")

	// 检测用户名是否存在
	var user model.User
	var count int64
	lib.Db.Where("username = ?", username).First(&user).Count(&count)
	if count > 0 {
		return nil, consts.CurdSelectFailCode, errors.New("用户已存在，请更换用户名后重试")
	}

	// 新增用户数据，注册逻辑
	passwordSalt := helper.RandStr(6)
	password = helper.Md5(helper.Md5(password) + passwordSalt)

	user = model.User{
		Username:     username,
		Password:     password,
		Nickname:     "新用户" + username,
		PasswordSalt: passwordSalt,
		RegisterTime: time.Now().Format("2006-01-02 15:04:05"),
		LoginTime:    time.Now().Format("2006-01-02 15:04:05"),
		Avatar:       viper.GetString("qiniu.default_avatar"),
		LastIp:       ctx.ClientIP(),
	}
	if err := lib.Db.Create(&user).Error; err != nil {
		lib.Logger.Debugf(err.Error())

		return nil, consts.CurdCreatFailCode, errors.New("服务器错误")
	}

	// 生成jwt token和用户信息给用户
	tokenString := jwt.MakeJwtToken(user.Id)

	return gin.H{
		"t": tokenString,
		"user": gin.H{
			"username": username,
			"nickname": user.Nickname,
			"avatar":   helper.QiNiuClient.FullPath(user.Avatar),
			"status":   user.Status,
		}}, consts.CurdStatusOkCode, nil

}

// Login 登录
func (s signCurd) Login(ctx *gin.Context) (map[string]interface{}, int, error) {
	username := ctx.GetString(consts.ValidatorPrefix + "username")
	password := ctx.GetString(consts.ValidatorPrefix + "password")

	// 检测用户是否存在
	var user model.User
	var count int64
	lib.Db.Where("username = ?", username).Select("id,username,password,password_salt,nickname,avatar,status").First(&user).Count(&count)
	if count < 1 {
		return nil, consts.CurdSelectFailCode, errors.New("用户不存在，请更换用户名后重试")
	}

	// 验证密码是否合法
	postPassword := helper.Md5(helper.Md5(password) + user.PasswordSalt)
	if postPassword != user.Password {
		return nil, consts.ValidatorParamsCheckFailCode, errors.New("密码错误")
	}

	// 密码正确，更新登陆时间
	loginTime := time.Now().Format("2006-01-02 15:04:05")
	lib.Db.Model(&user).Updates(map[string]interface{}{"login_time": loginTime, "last_ip": ctx.ClientIP()})

	// 生成jwt token和用户信息给用户
	tokenString := jwt.MakeJwtToken(user.Id)
	return gin.H{
		"t": tokenString,
		"user": gin.H{
			"username": username,
			"nickname": user.Nickname,
			"avatar":   helper.QiNiuClient.FullPath(user.Avatar),
			"status":   user.Status,
		},
	}, consts.CurdStatusOkCode, nil

}
