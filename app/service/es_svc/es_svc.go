package es_svc

import (
	"context"
	"github.com/olivere/elastic/v7"
	"strconv"
	"weIm-go/app/global/lib"
	"weIm-go/app/model"
)

var EsCurd = esCurd{}

type esCurd struct {
}

var userLocationMapping = `
{
	"settings":{
		"number_of_shards":2,
		"number_of_replicas":0
	},
	"mappings":{
		"properties":{
			"uid":{
				"type":"keyword"
			},	
		   "pin": {
				"properties": {
				  "location": {
					"type": "geo_point"
				  }
				}
		  }
		}
	}
}
`

// CreateIndexUserLocation  elasticsearch：用户位置index，用来创建index user_location索引
func (r *esCurd) CreateIndexUserLocation() error {
	exists, err := lib.Elasticsearch.IndexExists(model.UserLocationIndexName).Do(context.Background())
	if err != nil {
		return err
	}

	if !exists {
		_, err := lib.Elasticsearch.CreateIndex(model.UserLocationIndexName).Body(userLocationMapping).Do(context.Background())
		if err != nil {
			return err
		}
	}

	return nil
}

// InsertIndex 插入索引数据
func (r *esCurd) InsertIndex(id int, Latitude, Longitude float64) error {
	sId := strconv.Itoa(id)

	location := &model.Location{
		Location: elastic.GeoPointFromLatLon(Latitude, Longitude),
	}
	// 更新经纬度到es，用于后期查询
	userLocation := &model.UserLocation{
		Uid: sId,
		Pin: *location,
	}

	_, err := lib.Elasticsearch.Index().
		Index(model.UserLocationIndexName).
		Id(sId).
		BodyJson(&userLocation).
		Do(context.Background())

	if err != nil {
		return err
	}

	return nil
}
