package ws

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"net/http"
	"time"
	"weIm-go/app/global/lib"
	"weIm-go/app/service/jwt"
)

// WebsocketUpgrade 升级http为websocket服务
var WebsocketUpgrade = websocket.Upgrader{
	ReadBufferSize:   1024,
	WriteBufferSize:  1024,
	HandshakeTimeout: 5 * time.Second,
	// 取消ws跨域校验
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// WebsocketEntry websocket服务入口
func WebsocketEntry(ctx *gin.Context) {
	var conn *websocket.Conn
	var err error
	conn, err = WebsocketUpgrade.Upgrade(ctx.Writer, ctx.Request, nil) // 升级为websocket协议
	if err != nil {
		lib.Logger.Debugf(err.Error())
		return
	}

	var c Connection
	var counter int

	ClientCounterLocker.Lock()

	c.Conn = conn
	// 全局 连接计数器
	ClientCounter++
	counter = ClientCounter
	Connections[counter] = &c

	ClientCounterLocker.Unlock()

	// 返回 192.168.1.68:9901@@3
	c.ClientId = GenClientId(counter)

	// 15秒内没收到token绑定成功的断开连接
	time.AfterFunc(15*time.Second, func() {
		if c.Uid == 0 {
			_ = conn.Close()
		}
	})

	// 必须死循环，gin通过协程调用该handler函数，一旦退出函数，ws会被主动销毁
	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			lib.Logger.Debugf(err.Error())
			break
		}

		// json消息转换
		var message Message
		if err := json.Unmarshal(msg, &message); err != nil {
			// 接收到非json数据
			lib.Logger.Debugf(err.Error())

			_ = conn.WriteJSON(Message{
				Cmd:  CmdFail,
				Body: "消息格式错误",
				Ope:  OpeSystem,
				Type: TypePrompt,
			})

			continue
		}

		// 登录绑定uid和client_id，这是必须绑定的才能通信的
		if message.Cmd == CmdSign {
			if c.Uid == 0 {
				// id返回的是数据库users的id
				id, err := jwt.ParseToken(ctx, message.Body)
				if err != nil {
					_ = conn.WriteJSON(Message{
						Cmd:  CmdFail,
						Body: "认证失败",
						Ope:  OpeSystem,
						Type: TypePrompt,
					})
				}
				c.Uid = id

				UidToClientIdLocker.Lock()

				// UidToClientId map[7:192.168.1.68:9901@@1]  7对应的是 数据库user的id
				UidToClientId[c.Uid] = c.ClientId // 认证成功后注册到已认证连接表，方便查询对应clientId
				UidToClientIdLocker.Unlock()

				_ = conn.WriteJSON(Message{
					Cmd:  CmdSignSuccess,
					Body: "认证成功",
					Ope:  OpeSystem,
					Type: TypePrompt,
				})
			}
		}

		if c.Uid < 1 {
			_ = conn.WriteJSON(Message{
				Cmd:  CmdFail,
				Body: "你还未认证",
				Ope:  OpeSystem,
				Type: TypePrompt,
			})
		}
	}

	// 读写锁
	ClientCounterLocker.Lock()
	// 删除退出 websocket
	delete(Connections, counter)
	if c.Uid > 0 {
		UidToClientIdLocker.Lock()
		// 删除 UidToClientId 已认证连接表
		delete(UidToClientId, c.Uid)
		UidToClientIdLocker.Unlock()
	}
	ClientCounterLocker.Unlock()
}
