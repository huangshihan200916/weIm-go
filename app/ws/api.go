package ws

import (
	"github.com/gin-gonic/gin"
	"weIm-go/app/global/lib"
	"weIm-go/app/utils/response"
)

// StatusApi websocket本机服务状态
// {"code":0,"data":{"connections":{"1":{"ClientId":"192.168.1.68:9901@@1","Uid":7,"Conn":{}},"2":{"ClientId":"192.168.1.68:9901@@2","Uid":8,"Conn":{}}},"online":2,"uidToClientId":{"7":"192.168.1.68:9901@@1","8":"192.168.1.68:9901@@2"}},"msg":"status"}
func StatusApi(ctx *gin.Context) {
	response.Success(ctx, "status", gin.H{
		"connections":   Connections,
		"uidToClientId": UidToClientId,
		"online":        len(Connections),
	})
}

// SendToAll 发送给所有客户端测试性能
func SendToAll(ctx *gin.Context) {
	for _, v := range Connections {
		err := v.Conn.WriteJSON(Message{Cmd: -1, Body: "测试消息收发"})
		if err != nil {
			lib.Logger.Debugf(err.Error())
		}
	}
}
