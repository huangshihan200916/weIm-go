package etcd

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jianfengye/collection"
	"sync"
	"weIm-go/app/utils/response"
)

var Servers []string
var ServersLock sync.RWMutex

// AddServer 添加服务器
func AddServer(server string) {
	ServersLock.Lock()

	if len(Servers) < 1 {
		fmt.Println("[RPC-debug] add", server, "to ", Servers)
		Servers = append(Servers, server)
		return
	}

	// 判断server是否在集合Servers， 不存在就加进来
	strColl := collection.NewStrCollection(Servers)
	if strColl.Contains(server) != true {
		fmt.Println("[RPC-debug] add", server, "to ", Servers)
		Servers = append(Servers, server)
	}

	ServersLock.Unlock()

	return
}

// DelServer 删除服务器
func DelServer(server string) {
	ServersLock.Lock()
	for i := 0; i < len(Servers); i++ {
		if Servers[i] == server {
			fmt.Println("[RPC-debug] Del", server, "from ", Servers)

			Servers = append(Servers[:i], Servers[i+1:]...)
			i--
		}
	}
	ServersLock.Unlock()
}

// Api 查询服务器接口
func Api(ctx *gin.Context) {
	response.Success(ctx, "status", gin.H{
		"servers": Servers,
	})
}
