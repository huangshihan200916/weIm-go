package model

import "github.com/olivere/elastic/v7"

const (
	UserLocationIndexName = "user_location"
)

// UserLocation 用户位置 index 数据格式，用来json解析es查询结果
type UserLocation struct {
	Uid string   `json:"uid"`
	Pin Location `json:"pin"`
}

type Location struct {
	Location *elastic.GeoPoint `json:"location"`
}
