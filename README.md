## **分布式websocket服务：weIm-go**

**语言：Golang**

### **特性**

1. 集群服务注册发现
2. 分布式连接管理
3. websocket协议支持各种平台

### **环境要求**

1. Etcd
2. Mysql8.0.25+
3. Redis6.0.9+
4. ElasticSearch7.12.0+
5. 七牛云对象存储

### **运行**

1. 部署elasticsearch、redis、mysql、etcd
2. 导入 database.sql 到mysql
3. 修改配置文件config.yml
4. go run main.go

### **分布式部署**

1. 启动多个节点服务
2. nginx负载均衡

### **pprof性能监控访问**

1. 配置项：http.pprof
2. web访问：http://ip:port/debug/pprof

### **websocket并发性能测试**

执行并发测试：go run ./test/ws.client.go；
建议linux下进行并发测试，默认并发10000个客户端连接websocket并发送认证消息，可手动修改并发客户端数量。
测试环境：
i5-1035G4，WIN10系统
测试结果：
3万连接广播消息耗时平均0.8s，占用内存1.2G，测试路由：/ws/sendToAll
仅供参考，请自行部署测试。

### **API接口列表**

1. 用户登录
2. 用户注册
3. 设置昵称
4. 设置头像
5. 实名认证
6. 上传用户位置(经纬度)
7. 附近的人
8. 搜索用户
9. 好友列表
10. 添加好友
11. 添加好友请求列表
12. 同意/拒绝好友请求
13. 删除好友
14. **发送好友消息**
15. **撤回好友消息**
16. 创建群组
17. 搜索群组
18. 我的群组
19. 申请加群
20. 申请加群请求列表
21. 同意/拒绝加群请求
22. 退出群组
23. 踢出群组
24. **发送群组消息**
25. **撤回群组消息**
26. 上传聊天附件：语音、视频、文件格式




### **文档说明**

#### **一、Api接口**

登录：/api/v1/login
参数：username `string`
参数：password `string`

注册：/api/v1/register
参数：username `string`
参数：password `string`


登录/注册成功后会返回token，其他接口和websocket授权都需要带上token；api 接口在header添加 Login-Token 字段，websocket在连接后发送一个token认证信息进行认证，15秒之内没有认证成功会强制断开连接。

#### **二、websocket长连接**

连接地址：ws://ip:port/ws（例如`ws://127.0.0.1:8080/ws`）
登录成功后进行连接认证：

#### **通信消息JSON格式**

```json
{
    "Cmd": "指令",
    "FromId": "来源id",
    "ToId": "接收id",
    "Ope": "消息通道",
    "Type": "消息类型",
    "Body": "消息内容"
}
```
```tex
字段说明：
Cmd：指令:
    CmdFail 2 通用失败
    CmdSign 3 登录
    CmdSignSuccess 4 登录成功
    CmdReceiveFriendMessage 6 收到好友消息
    CmdWithdrawFriendMessage 7 撤回好友消息
    CmdReceiveFriendAdd 8 收到好友添加请求
    CmdReceiveFriendAddResult 9 收到好友请求结果
    CmdReceiveGroupMessage 10 收到群消息
    CmdWithdrawGroupMessage 11 撤回群消息
    CmdReceiveGroupJoin 12 收到加入群组请求
    CmdReceiveGroupJoinResult 13 收到加入群组结果
    CmdReceiveGroupShot 14 收到被踢出群组通知
FromId：消息发送方id
ToId：消息接收方id，ope=0时为用户id，ope=1是为群组id
Ope：消息通道，OpeFriend 0好友消息，OpeGroup 1群消息, OpeSystem 2系统消息
Type：消息类型，
    TypeText 0 文本消息，
    TypePicture 1 图片，
    TypeVoice 2 语音，
    TypeVideo 3 视频，
    TypeGeo 4 地理位置信息，
    TypeFile 6 文件，
    TypePrompt10 提示消息
Body：消息内容，登录认证时填用户Token
```



#### **用户连接websocket后发送登录认证消息**

```json
{
    "Cmd": 3,
    "FromId": 0,
    "ToId": 0,
    "Ope": 0,
    "Type": 0,
    "Body": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdCI6MTYxOTE0Mzg0OSwiaWQiOjV9.KQ7dOv6bE_fP5NpMehziesFMsZXDAdVrbYBHyZROw40"
}
```

#### 登录成功会收到消息：

```json
{
    "Cmd": 4,
    "FromId": 0,
    "ToId": 0,
    "Ope": 0,
    "Type": 0,
    "Body": "认证成功"
}
```

